App.store.capStore = new GeoExt.data.WMSCapabilitiesStore({
        url: /*App.store.proxy + */App.localWMSStore + App.store.capParameters
    });

App.model.project = {};
App.model.project.layers = []; // Capas extra
App.model.project.centroidsLayer = {}; // Centroides de las zonas de demanda
App.model.project.busStopLayer = {}; // Capa de paradas de bus
App.model.project.zonesLayer = {}; // Capa de zonas de demanda
App.model.project.walkLayer = {}; // Capa de caminatas
App.model.project.routeLayer = {}; // Capa de recorridos
App.model.project.roadLayer = {}; // Capa de calles
App.model.project.demandPointsLayer = {}; // Capa de puntos de demanda
