App.controller.getZoneForBusEvent = function(featureBusEvt){
	var busStopGeom = featureBusEvt.feature.geometry;
	var zones = App.model.project.zonesLayer.features;
	
	for (var i = 0; i < zones.length; i++){
		if (busStopGeom.intersects(zones[i].geometry)){
			featureBusEvt.feature.attributes.zone = zones[i].fid.split('.')[1];
			break;
		}
	}
};

App.controller.busStopTopologyCheck = function(){
	var busStopFeatures = App.model.project.busStopLayer.features;
	var busStopToCheck = [];
	var busStopWfs = new OpenLayers.Protocol.WFS({
	    url: App.localWFSStore,
	    featureType: App.model.project.roadLayer.name.indexOf(':') == -1 ? 
	    		App.model.project.roadLayer.name : 
	    		App.model.project.roadLayer.name.split(':')[1],
	    featureNS: App.wfsNamespace,
	    geometryName: App.geometryName,
	    srsName: App.CRS});
	
	for (var i = 0; i < busStopFeatures.length; i++){
		busStop = busStopFeatures[i];
		if (busStop.state == OpenLayers.State.INSERT || busStop.state == OpenLayers.State.UPDATE){
			busStopToCheck.push(busStop);
		}
	}
	
	if (busStopToCheck.length > 0){
		App.controller.busStopCheck(busStopWfs, busStopToCheck);
	}
};

App.controller.busStopCheck =  function(busStopWfs, busStopToCheck){
	if (busStopToCheck.length == 0){
		var strat = App.controller.getSaveStrategy(App.model.project.busStopLayer);
        strat.save();		
	} else {
		var busStop = busStopToCheck.shift();
		
		busStopWfs.read({
			filter: new OpenLayers.Filter.Spatial({
				type: OpenLayers.Filter.Spatial.INTERSECTS,
				property: App.geometryName,
				value: OpenLayers.Geometry.Polygon.createRegularPolygon(busStop.geometry, 15, 20)
			}),
			callback: function(response){
				if (response.features.length == 0){
					alert('Existen paradas que no estan sobre la capa de calles');
				} else {
					App.controller.busStopCheck(busStopWfs, busStopToCheck);
				}
			}
		});		
	}
};
