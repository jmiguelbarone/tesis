App.view.addLayer.options = {
		addToProject: true,
		success: function(layer){},
		failure: function(layer){}
};

App.view.addLayer.form = new App.view.addLayer.addLayerComponent();

App.view.addLayer.form.on('addLayer', function(parLayer){
	var resLayer = null;
	
	switch (parLayer.type){
	case "WMS":
		resLayer = parLayer;
		break;
	case "JSON":
		App.controller.addServerJsonLayer(parLayer, {
			success: function(layer){
				resLayer = layer;
			},
			failure: function(layer){				
				App.view.addLayer.options.failure(layer);
				return;
			}
		});
		break;
	case "SHP":
		App.controller.addServerShpLayer(parLayer, {
			success: function(layer){
				layer.type = "WMS";
				resLayer = layer;
			},
			failure: function(layer){
				App.view.addLayer.options.failure(layer);
				return;
			}
		});
		break;
		default:
			console.log("Tipo de capa desconocido " + layer.type);
			App.view.addLayer.options.failure(layer);
			return;
	};
	
	App.controller.addLayerToMap(resLayer);
	
	if (App.view.addLayer.options.addToProject){
		App.controller.addLayerProject(resLayer);
	}
	App.view.addLayer.options.success(resLayer);
});
	

App.view.addLayer.getLayer = function(options){
	App.view.addLayer.options = options;
	if (typeof App.view.addLayer.options.addToProject == 'undefined'){
		App.view.addLayer.options.addToProject = true;
	}
	App.view.addLayer.form.show();
};
