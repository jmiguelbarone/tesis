Ext.ns('App.controller.project');

App.controller.newProject = function(formData){
	var proyect = formData.name;
	
	formData.centroidsLayer = Ext.decode('{"name": "' + proyect + '_centroides", "title":"Centroides", "baseLayer": false, "store": App.localWMSStore, "type": "WMS"}');
	formData.busStopLayer = Ext.decode('{"name": "' + proyect + '_paradas", "title":"Paradas", "baseLayer": false, "store": App.localWMSStore, "type": "WMS"}');
	formData.zonesLayer = Ext.decode('{"name": "' + proyect + '_zonas", "title":"Zonas", "baseLayer": false, "store": App.localWMSStore, "type": "WMS"}');
	formData.walkLayer = Ext.decode('{"name": "' + proyect + '_caminatas", "title":"Caminatas", "baseLayer": false, "store": App.localWMSStore, "type": "WMS"}');
	formData.routeLayer = Ext.decode('{"name": "' + proyect + '_recorridos", "title":"Recorridos", "baseLayer": false, "store": App.localWMSStore, "type": "WMS"}');

	App.controller.addServerFeatureType(
			formData.busStopLayer.title, 
			formData.busStopLayer.name, 
			[{name: App.geometryName, nillable: false, type: "com.vividsolutions.jts.geom.Point"},
			 {name: "zone", nillable: false, type: "java.lang.Integer"}], {
		success: function(){
			App.controller.addServerFeatureType(
					formData.centroidsLayer.title, 
					formData.centroidsLayer.name, 
					[{name: App.geometryName, nillable: false, type: "com.vividsolutions.jts.geom.Point"},
					 {name: "zone", nillable: false, type: "java.lang.Integer"}], {
				success: function(){
					App.controller.addServerFeatureType(
							formData.zonesLayer.title, 
							formData.zonesLayer.name, 
							[{name: App.geometryName, nillable: false, type: "com.vividsolutions.jts.geom.Polygon"}], {
						success: function(){
							App.controller.addServerFeatureType(
									formData.walkLayer.title, 
									formData.walkLayer.name, 
									[{name: App.geometryName, nillable: false, type: "com.vividsolutions.jts.geom.LineString"},
									 {name: "centroid", nillable: false, type: "java.lang.Integer"},
									 {name: "busStop", nillable: false, type: "java.lang.Integer"}], {
								success: function(){
									App.controller.addServerFeatureType(
											formData.routeLayer.title, 
											formData.routeLayer.name, 
											[{name: App.geometryName, nillable: false, type: "com.vividsolutions.jts.geom.MultiLineString"}], {
										success: function(){
											Ext.Ajax.request({
												url : App.restServicesBase + "/projects/save",
												method: 'POST',
												waitTitle: 'Conectando...',
												waitMsg: 'Enviando datos...',
												jsonData: formData,
												success: function(response){
													App.model.project = formData;
													App.model.project.centroidsLayer = App.controller.addLayerToMap(App.model.project.centroidsLayer, App.controller.createCentroidStyle());	
													App.model.project.busStopLayer = App.controller.addLayerToMap(App.model.project.busStopLayer, App.controller.createBusStopStyle());	
													App.model.project.zonesLayer = App.controller.addLayerToMap(App.model.project.zonesLayer, App.controller.createZonesStyle());	
													App.model.project.walkLayer = App.controller.addLayerToMap(App.model.project.walkLayer, App.controller.createWalkStyle());
													App.model.project.routeLayer = App.controller.addLayerToMap(App.model.project.routeLayer, App.controller.createRouteStyle());
								        	
													App.controller.setOLControls();
													App.controller.setSelectControls();
													App.view.updateMenues();			
												}
											});
										}
								    });	
								},
								failure: function(){
								}
							});
						},
						failure: function(){
						}
					});        

				},
				failure: function(){
				}
			});        
			
		},
		failure: function(){
		}
	});        	
};

App.controller.isProjectLoaded = function(){
	return (typeof App.model.project.name != 'undefined');
};

App.controller.loadProject = function(projectId, projectName){
	if (App.controller.isProjectLoaded()){
		App.controller.logToConsole("Limpiando proyecto actual...", "Abrir proyecto");
		App.controller.cleanProject();
	}
	
	Ext.Ajax.request({
        url : App.restServicesBase + "/projects/" + projectId,
        method: 'GET',
        waitTitle: 'Conectando...',
        waitMsg: 'Enviando datos...',
		success: function(response){
			App.model.project = Ext.decode(response.responseText);

			var layer = {};
			
			if (App.model.project.layers[0]){
				for (var i = 0; i < App.model.project.layers.length; i++){
					layer = App.model.project.layers[i];
					App.controller.logToConsole("Agregando capa " + layer.name, "Abrir proyecto");
					App.controller.addLayerToMap(layer);
				}		
			}
			
			if (App.model.project.roadLayer){
				App.model.project.roadLayer = App.controller.addLayerToMap(App.model.project.roadLayer);				
			}

			if (App.model.project.demandPointsLayer){
				layer = App.model.project.demandPointsLayer;
				App.controller.addLayerToMap(layer);				
			}

			if (App.model.project.busStopLayer){
				App.model.project.busStopLayer = App.controller.addVectorLayerToMap(App.model.project.busStopLayer, App.controller.createBusStopStyle());
			}			
			if (App.model.project.centroidsLayer){
				App.model.project.centroidsLayer = App.controller.addVectorLayerToMap(App.model.project.centroidsLayer, App.controller.createCentroidStyle());				
			}
			if (App.model.project.zonesLayer){
				App.model.project.zonesLayer = App.controller.addVectorLayerToMap(App.model.project.zonesLayer, App.controller.createZonesStyle());				
			}
			if (App.model.project.walkLayer){
				App.model.project.walkLayer = App.controller.addVectorLayerToMap(App.model.project.walkLayer, App.controller.createWalkStyle());				
			}
			if (App.model.project.routeLayer){
				App.model.project.routeLayer = App.controller.addVectorLayerToMap(App.model.project.routeLayer, App.controller.createRouteStyle());				
			}
			
        	App.controller.setOLControls();
        	App.controller.setSelectControls();
			App.view.updateMenues();	
			App.controller.logToConsole("Proyecto abierto correctamente", "Abrir proyecto");
		}        
	});
};

App.controller.cleanProject = function(){
	if (App.model.project.busStopLayer){
		App.map.removeLayer(App.model.project.busStopLayer);
		App.model.project.busStopLayer.destroyFeatures();
		App.model.project.busStopLayer.destroy();
	}			
	if (App.model.project.centroidsLayer){
		App.map.removeLayer(App.model.project.centroidsLayer);
		App.model.project.centroidsLayer.destroyFeatures();
		App.model.project.centroidsLayer.destroy();
	}
	if (App.model.project.zonesLayer){
		App.map.removeLayer(App.model.project.zonesLayer);
		App.model.project.zonesLayer.destroyFeatures();
		App.model.project.zonesLayer.destroy();
	}
	if (App.model.project.walkLayer){
		App.map.removeLayer(App.model.project.walkLayer);
		App.model.project.walkLayer.destroyFeatures();
		App.model.project.walkLayer.destroy();
	}	
/*
	if (App.model.project.roadLayer){
		App.map.removeLayer(App.model.project.roadLayer);
		App.model.project.roadLayer.destroyFeatures();
		App.model.project.roadLayer.destroy();
	}	
*/
	for (var i = 0, len = App.map.layers.length; i < len; i++){
		if (App.map.layers[i].params.LAYERS != App.baseLayerMvdLimits){
			App.map.removeLayer(App.map.layers[i]);
		}
	}

	if (App.model.project.roadLayer){
		App.map.removeLayer(App.model.project.roadLayer);				
	}
	if (App.model.project.demandPointsLayer){
		App.map.removeLayer(App.model.project.demandPointsLayer);				
	}

};

App.controller.setBusStopLayer = function(title, store, name, baseLayer){
	var layer = {"name": name, "title": title, "store": store,  
			"baseLayer": baseLayer, "type": "WMS"};
	
	App.controller.addWmsLayerToMap(layer);				
	App.model.project.busStopLayer = layer;
};

App.controller.closeProject = function(){
	//TODO: grabar cambios
	App.model.project = {};

	var delLayers = new Array();
	
	Ext.each(App.map.layers, function(layer){
		if (layer.isBaseLayer == false){
			delLayers.push(layer);
		}
	});
	
	Ext.each(delLayers, function(layer){
		App.map.removeLayer(layer);
	});
	App.view.updateMenues();
};

App.controller.saveProject = function(){
	Ext.Ajax.request({
        url : App.restServicesBase + "/projects/save",
        method: 'POST',
        waitTitle: 'Conectando...',
        waitMsg: 'Enviando datos...',
        jsonData: Ext.encode(App.model.project),
		success: function(response){
			App.controller.logToConsole("Proyecto " + App.model.project.name + " salvado", "Salvando proyecto...");
		},
		failure: function(response){
			App.controller.logToConsole(response, "Salvando proyecto " + App.model.project.name);			
		}
	});		
};

App.controller.addLayerProject = function(layer){
	var len = App.model.project.layers.length;
	
	App.model.project.layers[len] = layer;
};

App.controller.logToConsole = function(message, operation){
	var data = [{ "message": message, "project": App.controller.isProjectLoaded() ? App.model.project.name : "[Sin proyecto]", "operation": operation, "dateOperation": new Date()}];
	
	App.view.consoleStore.loadData(data, true);
};

App.controller.addLayer = function(){
	App.view.addLayer.getLayer({
		success: function(fileName){
			
		},
		failure: function(fileName){
			
		}
	});
};

App.controller.dump = function(){
	console.log("");
};

App.controller.createCentroidStyle = function(){
	return new OpenLayers.StyleMap({
		'default': new OpenLayers.Style({
							'strokeWidth': 2,
							'strokeColor': '#aaee77',
							'fillColor': '#669933',
							'pointRadius': 6
					})});
};
App.controller.createBusStopStyle = function(){
	return new OpenLayers.StyleMap({'default': new OpenLayers.Style({
		'strokeWidth': 1,
		'strokeColor': '#ff0000',
		'pointRadius': 4
	})});	
};
App.controller.createZonesStyle = function(){
	return new OpenLayers.StyleMap({'default': new OpenLayers.Style({
		'fill': true,
		'fillColor': "#ff00ff",
		'pointRadius': 6,
		'fillOpacity': .5
	})});
};
App.controller.createWalkStyle = function(){
	return new OpenLayers.StyleMap({'default': new OpenLayers.Style({
		'fill': true,
		'pointRadius': 4,
//        graphicName: "square",
        'fillColor': 'yellow',
//        'fillOpacity': 0.25,
		'strokeWidth': 2,
		'strokeDashstyle': 'dash',
		'strokeColor': 'yellow'
	})});
};
App.controller.createRouteStyle = function(){
		return new OpenLayers.StyleMap({'default': new OpenLayers.Style({
			'fill': true,
			'pointRadius': 4,
//	        graphicName: "square",
	        'fillColor': 'green',
//	        'fillOpacity': 0.25,
			'strokeWidth': 2,
			'strokeDashstyle': 'dash',
			'strokeColor': 'green'
	})});
};

App.controller.createRoadStyle = function(){
	return new OpenLayers.StyleMap({'default': new OpenLayers.Style({
		'fill': true,
		'pointRadius': 4,
//        graphicName: "square",
        'fillColor': 'blue',
//        'fillOpacity': 0.25,
		'strokeWidth': 2,
		'strokeColor': 'blue'
})});
};
