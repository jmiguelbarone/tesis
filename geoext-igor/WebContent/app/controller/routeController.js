// Recibe un tramo de calle con el que debe ser generado un tramo de recorrido

App.controller.routeCheck = function(event){
	var routeSegment = new OpenLayers.Feature.Vector(event.feature.geometry);
	routeSegment.state = OpenLayers.State.INSERT;
	
	App.model.project.routeLayer.addFeatures([routeSegment]);
//	App.model.project.routeLayer.drawFeature(routeSegment);
};

