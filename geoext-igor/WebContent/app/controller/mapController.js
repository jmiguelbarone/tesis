App.view.busStopEditPanel;
App.view.zonesEditPanel;
App.view.walkEditPanel;

App.controller.setSelectControls = function(){
	var controls = App.controller.getAllSelectControls();
	for (var i = 0; i < controls.length; i++){
		App.map.removeControl(controls[i]);
	}
	
	var selectControl = new OpenLayers.Control.SelectFeature(App.model.project.busStopLayer);
	App.map.addControl(selectControl);
	
	selectControl = new OpenLayers.Control.SelectFeature(App.model.project.zonesLayer);
	App.map.addControl(selectControl);

	selectControl = new OpenLayers.Control.SelectFeature(App.model.project.walkLayer);
	App.map.addControl(selectControl);
	
	selectControl = new OpenLayers.Control.SelectFeature(App.model.project.centroidsLayer);
	App.map.addControl(selectControl);

	/*
	selectControl = new OpenLayers.Control.SelectFeature(App.model.project.routeLayer);
	App.map.addControl(selectControl);
	*/
};

App.controller.setOLControls = function(){
	if (App.view.busStopEditPanel){
		App.control.destroyPanel(App.view.busStopEditPanel);
	}
	App.view.busStopEditPanel = App.controller.createPanel(App.model.project.busStopLayer, App.controller.busStopTopologyCheck, OpenLayers.Handler.Point, ' paradas');  
	App.map.addControl(App.view.busStopEditPanel);


	if (App.view.zonesEditPanel){
		App.control.destroyPanel(App.view.zonesEditPanel);
	}
	App.view.zonesEditPanel = App.controller.createPanel(App.model.project.zonesLayer, App.controller.zoneTopologyCheck, OpenLayers.Handler.Polygon, ' zonas');  
	App.map.addControl(App.view.zonesEditPanel);
	
	// Agrego FID de la zona a una parada nueva
	App.model.project.busStopLayer.events.register('beforefeatureadded', undefined, App.controller.getZoneForBusEvent);
	// Corrijo FID de la zona a una parada modificada
	App.model.project.busStopLayer.events.register('featuremodified', undefined, App.controller.getZoneForBusEvent);
	
	App.model.project.zonesLayer.events.register('beforefeatureadded', undefined, App.controller.zoneAddHandler);
	App.model.project.zonesLayer.events.register('beforefeatureremoved', undefined, App.controller.zoneDelHandler);
	App.model.project.zonesLayer.events.register('featuremodified', undefined, App.controller.zoneUpdHandler);

	// Se crean centroides de nuevas zonas y se salvan cambios (updates /  delete / create de centroides)
	App.controller.getSaveStrategy(App.model.project.zonesLayer).events.register('success', undefined, App.controller.zoneToCentroid);
	
	if (App.view.walkEditPanel){
		App.control.destroyPanel(App.view.walkEditPanel);
	}
	App.view.walkEditPanel = App.controller.createPanel(App.model.project.walkLayer, App.controller.walkTopologyCheck, OpenLayers.Handler.Path, ' caminatas');  
	App.map.addControl(App.view.walkEditPanel);	

	if (App.view.routeEditPanel){
		App.control.destroyPanel(App.view.routeEditPanel);
	}
	App.view.routeEditPanel = App.controller.createRoutePanel(App.model.project.routeLayer, App.controller.routeCheck, ' recorridos');  
	App.map.addControl(App.view.routeEditPanel);	
};

App.controller.destroyPanel = function(panel){
	App.map.removeControl(panel);
	Ext.each(panel.controls, function(control, index){
		control.destroy();
	});
	panel.destroy();	
};

App.controller.createPanel = function(layer, saveCheck, handler, text){
	var navigate = new OpenLayers.Control.Navigation({title: 'Navegacion'});
	var panel = new OpenLayers.Control.Panel({
		autoActivate: false, 
		displayClass: 'EditingPanel',
		defaultControl: navigate
	});
	
	panel.addControls([
  	   new OpenLayers.Control.Button({
  		   displayClass: 'olControlSaveFeatures', 
  		   trigger: function() {
  			   saveCheck();
	       }, 
	       title: 'Salvar ' + text 
	   }),
	   new DeleteFeature(layer, {title: 'Eliminar ' + text}),
	   new OpenLayers.Control.ModifyFeature(layer, {title: 'Modificar ' + text}),
	   new OpenLayers.Control.DrawFeature(layer, handler, {title: 'Crear ' + text}),
	   navigate
	]);
	
	return panel;
};

App.controller.createRoutePanel = function(layer, check, text){
	var navigate = new OpenLayers.Control.Navigation({title: 'Navegacion'});
	var panel = new OpenLayers.Control.Panel({
		autoActivate: false, 
		displayClass: 'EditingPanel',
		defaultControl: navigate
	});
	/*
	var selectRoads = new OpenLayers.Control.SelectFeature(
			App.model.project.roadLayer, 
			{title: 'Crear ' + text,
			 onSelect: check,
			 displayClass: 'olControlDrawFeature'
			 }
	);
	*/
	
	var selectRoads = new OpenLayers.Control.GetFeature({
	    protocol: OpenLayers.Protocol.WFS.fromWMSLayer(App.model.project.roadLayer, 
	    		{url: App.localWFSStore}
	    ),
		title: 'Crear ' + text,
		clickout: false, // no deselecciona si se hace click afuera
		toggle: true, // deselecciona haciendo click encima de algo seleccionado
		displayClass: 'olControlDrawFeature'
	});
	
	selectRoads.events.register("featureselected", this, check);
	
	panel.addControls([
  	   new OpenLayers.Control.Button({
  		   displayClass: 'olControlSaveFeatures', 
  		   trigger: function() {
  				var strat = App.controller.getSaveStrategy(layer);
  			    strat.save();		
	       }, 
	       title: 'Salvar ' + text 
	   }),
	   new DeleteFeature(layer, {title: 'Eliminar ' + text}),
	   selectRoads,
	   navigate
	]);
	
	return panel;
};

App.controller.getSaveStrategy = function(vector){
	return OpenLayers.Array.filter(vector.strategies, function(s) {
		   return s instanceof OpenLayers.Strategy.Save;
	})[0];
};

App.controller.getSelectControl = function(vector){
	return OpenLayers.Array.filter(App.map.controls, function(s) {
		   return s instanceof OpenLayers.Control.SelectFeature && s.layer == vector;
	})[0];
};

App.controller.getAllSelectControls = function(){
	return OpenLayers.Array.filter(App.map.controls, function(s) {
		   return s instanceof OpenLayers.Control.SelectFeature;
	});
};

var DeleteFeature = OpenLayers.Class(OpenLayers.Control, {

    initialize: function(layer, options) {
        OpenLayers.Control.prototype.initialize.apply(this, [options]);
        this.layer = layer;
        this.handler = new OpenLayers.Handler.Feature(
            this, layer, {click: this.clickFeature}
        );
    },

    clickFeature: function(feature) {
    	feature.state = OpenLayers.State.DELETE;
        this.layer.events.triggerEvent(
        		"beforefeatureremoved", {feature: feature}
        );
        this.layer.drawFeature(feature);
    },

    setMap: function(map) {
        this.handler.setMap(map);
        OpenLayers.Control.prototype.setMap.apply(this, arguments);
    },

    CLASS_NAME: "DeleteFeature"
});