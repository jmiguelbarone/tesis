// Chequeo que la caminata comience en un centroide y termine en una parada dentro de la misma zona

App.controller.walkTopologyCheck = function(){
	var features = App.model.project.walkLayer.features;
	var walkArray = [];
	var initWfs = new OpenLayers.Protocol.WFS({
	    url: App.localWFSStore,
	    featureType: App.model.project.centroidsLayer.name.indexOf(':') == -1 ? 
	    		App.model.project.centroidsLayer.name : 
		    		App.model.project.centroidsLayer.name.split(':')[1],
	    featureNS: App.wfsNamespace,
	    geometryName: App.geometryName,
	    srsName: App.CRS
	});
	var endWfs = new OpenLayers.Protocol.WFS({
	    url: App.localWFSStore,
	    featureType: App.model.project.busStopLayer.name.indexOf(':') == -1 ? 
	    		App.model.project.busStopLayer.name : 
		    		App.model.project.busStopLayer.name.split(':')[1],
	    featureNS: App.wfsNamespace,
	    geometryName: App.geometryName,
	    srsName: App.CRS
	});
	
	var sameZoneWfs = new OpenLayers.Protocol.WFS({
	    url: App.localWFSStore,
	    featureType: App.model.project.zonesLayer.name.indexOf(':') == -1 ? 
	    		App.model.project.zonesLayer.name : 
		    		App.model.project.zonesLayer.name.split(':')[1],
	    featureNS: App.wfsNamespace,
	    geometryName: App.geometryName,
	    srsName: App.CRS
	});

	for (var i = 0; i < features.length; i++){
		if (features[i].state == OpenLayers.State.INSERT || features[i].state == OpenLayers.State.UPDATE){
			walkArray.push(features[i]);
		}
	}
	
	App.controller.walkCheck(initWfs, endWfs, sameZoneWfs, walkArray);
};

App.controller.walkCheck = function(initWfs, endWfs, sameZoneWfs, walkArray){
	if (walkArray.length == 0){
		var strat = App.controller.getSaveStrategy(App.model.project.walkLayer);
	    strat.save();		
	} else {
		var walk = walkArray.shift();
		var init = walk.geometry.components[0];
		var end = walk.geometry.components[walk.geometry.components.length - 1];
		var initBuffer = OpenLayers.Geometry.Polygon.createRegularPolygon(init, 15, 20);
		var endBuffer = OpenLayers.Geometry.Polygon.createRegularPolygon(end, 15, 20);
		
		initWfs.read({
			filter: new OpenLayers.Filter.Spatial({type: OpenLayers.Filter.Spatial.INTERSECTS, property: App.geometryName,
												   value: initBuffer}),
			callback: function(response){
				if (response.features.length == 0){
					alert('Existen caminatas que no comienzan en centroide');
				} else {
					walk.attributes.centroid = response.features[0].fid.split('.')[1];
					endWfs.read({
						filter: new OpenLayers.Filter.Spatial({type: OpenLayers.Filter.Spatial.INTERSECTS, property: App.geometryName,
							   value: endBuffer}),
						callback: function(response){
							if (response.features.length == 0){
								alert('Existen caminatas que no terminan en una parada');
							} else {
								walk.attributes.busStop = response.features[0].fid.split('.')[1];
								sameZoneWfs.read({
									filter: new OpenLayers.Filter.Spatial({type: OpenLayers.Filter.Spatial.INTERSECTS, property: App.geometryName,
										   value: walk.geometry}),
									callback: function(response){
										if (response.features.length == 0 || response.features.length > 1){
											alert('Existen caminatas que no estan en una zona o en mas de una');
										} else {
											App.controller.walkCheck(initWfs, endWfs, sameZoneWfs, walkArray);
										}
									}
								});
							}
						}
					});
				}
			}
		});
	}	
};
