
//title, store, name, isBase

App.controller.addLayerToMap = function(layer){
	var olLayer = undefined;
	
	switch (layer.type){
	case "WMS":
		olLayer = App.controller.addWmsLayerToMap(layer);
		break;
	case "JSON":
		olLayer = App.controller.addJsonLayerToMap(App.model.project.name, layer.name);
		break;
	default:
		console.log("Tipo de capa no implementado " + layer.type);
		break;
	};

	return olLayer;
};

App.controller.addVectorLayerToMap = function(layer, style){
	var saveStrategy = new OpenLayers.Strategy.Save();
	saveStrategy.events.on({
		'success': function(event){
			console.log("cambios salvados en " + event.object.layer.name);
		},
		'fail': function(event){
			console.log("fallo al salvar los cambios en " + event.object.layer.name);			
		},
		scope: this
	});
	
	var vector = new OpenLayers.Layer.Vector(layer.name, {
		isBaseLayer : false,
		styleMap: style,
		strategies : [ new OpenLayers.Strategy.BBOX(), saveStrategy ], 
		protocol : new OpenLayers.Protocol.WFS({
			url : App.localWFSStore,
			featureType : layer.name,
			featureNS : App.wfsNamespace,
			srsName : App.CRS,
			version : "1.1.0",
			geometryName: App.geometryName
		}) 		
	});
	
	vector.displayInLayerSwitcher = (typeof layer.display == 'undefined') ? true : layer.display;
	App.map.addLayer(vector);
	
	return vector;
};

App.controller.addWmsLayerToMap = function(layer){
	var baseLayer = (typeof layer.baseLayer == 'undefined') ? false : layer.baseLayer;
	var display = (typeof layer.display == 'undefined') ? true : layer.display;
	
	var wms = new OpenLayers.Layer.WMS(
	        layer.title,
	        layer.store,
	        {layers: layer.name, transparent: !baseLayer, tiled: true, 
	         tilesOrigin : App.map.maxExtent.left + ',' + App.map.maxExtent.bottom},
	        {buffer: 0, isBaseLayer: baseLayer, displayInLayerSwitcher: display,
	         displayOutsideMaxExtent: true
//	         , yx : { App.CRS : false}
	         }
	);
	
	App.map.addLayer(wms);
	
	return wms;
};

App.controller.addJsonLayerToMap = function(project, fileName){
	var layer = new OpenLayers.Layer.Vector(fileName,{
		projection: new OpenLayers.Projection(App.CRS),
		strategies: [new OpenLayers.Strategy.Fixed()],
		protocol: new OpenLayers.Protocol.HTTP({
			url: App.geojsonService + '?name=' + fileName + '&project=' + project,
			format: new OpenLayers.Format.GeoJSON()
		})
	});
	
	App.map.addLayer(layer);
	
	return layer;
};

App.controller.addServerJsonLayer = function(layer, options){
	var reader = new FileReader();
	
	reader.onloadend = function(evt){
		if (evt.target.readyState == FileReader.DONE) {
			var res = {"title": layer.title, "name": layer.name, "baseLayer": layer.baseLayer};

			Ext.Ajax.request({
		        url : App.postGeojsonService,
		        method: 'POST',
		        params: {
		        	'project': App.model.project.name,
		        	'name': layer.name
		        },
		        waitTitle: 'Conectando...',
		        waitMsg: 'Enviando datos...',
		        jsonData: evt.target.result,
		        success: options.success(res),
		        failure: options.failure(res)
		    });	
		}
	};
	reader.readAsText(layer.store);
};

App.controller.addServerFeatureType = function(title, name, atts, options){
	var data =	'<featureType> \
		  <name>' + name + '</name> \
		  <title>' + title + '</title> \
		  <nativeCRS class="projected">' + App.nativeCRS +
	     '</nativeCRS> \
		  <srs>' + App.CRS + '</srs> \
		  <nativeBoundingBox>' + App.nativeBoundingBox +
		    '<crs class="projected">' + App.CRS +'</crs> \
		  </nativeBoundingBox> \
		  <latLonBoundingBox>' + App.latLonBoundingBox +
		    '<crs>GEOGCS[&quot;WGS84(DD)&quot;,DATUM[&quot;WGS84&quot;, \
		    SPHEROID[&quot;WGS84&quot;, 6378137.0, 298.257223563]],PRIMEM[&quot;Greenwich&quot;, 0.0],UNIT[&quot;degree&quot;, 0.017453292519943295], \
		  AXIS[&quot;Geodetic longitude&quot;, EAST], \
		  AXIS[&quot;Geodetic latitude&quot;, NORTH]]</crs></latLonBoundingBox> \
		  <projectionPolicy>FORCE_DECLARED</projectionPolicy> \
		  <enabled>true</enabled> \
		  <attributes>';


	Ext.each(atts, function(att, index){
		data = data + '<attribute> \
	      <name>' + att.name + '</name> \
	      <minOccurs>0</minOccurs> \
	      <maxOccurs>1</maxOccurs> \
	      <nillable>' + att.nillable + '</nillable> \
	      <binding>' + att.type + '</binding> \
	    </attribute>';
	});
	
	data = data + '</attributes> \
	</featureType>';

	Ext.Ajax.request({
        url : App.geoserver + '/rest/workspaces/' + App.localWks + '/datastores/' + App.gsDBStore + '/featuretypes.xml',
        method: 'POST',
        headers: {
        	'Content-Type': 'application/xml',
        	'Authorization': App.geoserverRestAuth
        },
        waitTitle: 'Conectando...',
        waitMsg: 'Enviando datos...',
        xmlData: data,
        success: options.success,
        failure: options.failure
    });
};

App.controller.addServerShpLayer = function(layer, options){
	var reader = new FileReader();
	
	reader.onloadend = function(evt){
		if (evt.target.readyState == FileReader.DONE) {
			
			var binaryData = evt.target.result;

			var xhr = new XMLHttpRequest();
			xhr.open('PUT', App.geoserver + '/rest/workspaces/' + App.localWks + '/datastores/' + layer.name + '/file.shp', true);
			xhr.onload = function(e){
				var res = {"title": layer.title, "store": App.geoserver + App.localWMSStore,
						"name": layer.name, "baseLayer": layer.baseLayer};
				if (this.status == 201){
					options.success(res);
				} else {
					options.failure(res);
				}
			};
			xhr.setRequestHeader('Content-type', 'application/zip');
			xhr.setRequestHeader('Authorization', App.geoserverRestAuth);
			xhr.sendAsBinary(binaryData);
		}
	};
	reader.readAsBinaryString(layer.store);
};
