App.controller.zoneTopologyCheck = function(){
	var features = App.model.project.zonesLayer.features;
	var zonesToCheck = [];
	var zoneWfs = new OpenLayers.Protocol.WFS({
	    url: App.localWFSStore,
	    featureType: App.model.project.zonesLayer.name.indexOf(':') == -1 ? 
	    		App.model.project.zonesLayer.name : 
		    		App.model.project.zonesLayer.name.split(':')[1],
	    featureNS: App.wfsNamespace,
	    geometryName: App.geometryName,
	    srsName: App.CRS
	});

	for (var i = 0; i < features.length; i++){
		if (features[i].state == OpenLayers.State.INSERT || features[i].state == OpenLayers.State.UPDATE){
			zonesToCheck.push(features[i]);
		}		
	}

	if (zonesToCheck.length > 0){ 
		App.controller.checkZones(zoneWfs, zonesToCheck);
	}
};

App.controller.checkZones = function(zoneWfs, zonesToCheck){
	if (zonesToCheck.length == 0){
		var strat = App.controller.getSaveStrategy(App.model.project.zonesLayer);
	    strat.save();				
	} else {
		var zone = zonesToCheck.shift();
		
		zoneWfs.read({
			filter: App.controller.createWfsFilter(zone), 
			callback: function(response){
				if (response.features.length > 0){
					alert('Existen zonas solapadas!!!');
				} else {
					App.controller.checkZones(zoneWfs, zonesToCheck);
				}
			}
		});		
	}	
};

// si es insert no existe la zona todavia y no existe FID de la misma
App.controller.createWfsFilter = function(zone){
	var spatialFilter = new OpenLayers.Filter.Spatial({
		type: OpenLayers.Filter.Spatial.INTERSECTS,
		property: App.geometryName,
		value: zone.geometry
	});
	
	var filter = (zone.state == OpenLayers.State.INSERT) ? spatialFilter :
		new OpenLayers.Filter.Logical({
		filters: [
		    spatialFilter, 
		    new OpenLayers.Filter.Logical({
		    	filters: [new OpenLayers.Filter.FeatureId({fids: [zone.fid.split('.')[1]]})], 
		    	type: OpenLayers.Filter.Logical.NOT
		    })
		],
		type: OpenLayers.Filter.Logical.AND
	});
	
	return filter;
};

App.controller.zoneToCentroid = function(saveEvent){
	var ids = saveEvent.response.insertIds;
	for (var i = 0; i < ids.length; i++){
		if (ids[i] == 'none') continue;
		var zone = App.model.project.zonesLayer.getFeatureByFid(ids[i]);
		if (!zone) continue;
			
		var centroid = zone.geometry.getCentroid();
		var featureCentroid = new OpenLayers.Feature.Vector(centroid);
		featureCentroid.state = OpenLayers.State.INSERT;
		featureCentroid.zone = ids[i].split('.')[1];
					
		App.model.project.centroidsLayer.addFeatures([ featureCentroid ]);
	};		
	App.controller.getSaveStrategy(App.model.project.centroidsLayer).save();
};

App.controller.zoneAddHandler = function(feature){
	if (!App.view.zonesEditPanel.active) return;
	
};

App.controller.zoneDelHandler = function(feature){
	if (!App.view.zonesEditPanel.active) return;
	
	// No existe centroide hasta que no este salvada la zona
	if (!feature.feature.fid) return;
	var zoneFid = feature.feature.fid.split('.')[1];
	
	var centroid = App.model.project.centroidsLayer.getFeatureBy('zone', zoneFid);
	if (centroid){
		centroid.state = OpenLayers.State.DELETE;
		App.model.project.centroidsLayer.drawFeature(centroid);
	};
};

App.controller.zoneUpdHandler = function(feature){
	if (!App.view.zonesEditPanel.active) return;

	// No existe centroide hasta que no este salvada la zona
	if (feature.feature.state == OpenLayers.State.INSERT) return;

	var zoneFid = feature.feature.fid.split('.')[1];
	var centroid = App.model.project.centroidsLayer.getFeatureBy('zone', zoneFid);
	
	if (centroid){
		var geomCentroid = feature.feature.geometry.getCentroid();
		centroid.geometry.x = geomCentroid.x;
		centroid.geometry.y = geomCentroid.y;
		centroid.state = OpenLayers.State.UPDATE;
		App.model.project.centroidsLayer.drawFeature(centroid);
	}
};
