Ext.ns('App.controller.matrixController');

App.controller.matrixController.show = function(show){
	if (show){
		//App.view.matrixView.matrixStore.getProxy().url = App.restServicesBase + '/projects/matrix/' + App.model.project.id;
//		App.view.matrixView.matrixStore.proxy = new Ext.data.HttpProxy({
//			url: App.restServicesBase + '/projects/matrix/' + App.model.project.id,
//			method: 'GET'
//		}); 
		
		Ext.Ajax.request({
			url: App.restServicesBase + '/projects/matrix/' + App.model.project.id,
			method: 'GET',
			success: function(response, options){
				var matrix = {};
				
				if (response.status == 204){ // No hay datos
					matrix = App.controller.matrixController.createEmptyMatrix(App.model.project.id);
				} else {
					matrix = Ext.decode(response.responseText);					
				}
				
				App.view.matrixView.showMatrices.showMatrix(matrix);
			},
			failure: function(response, options){
				console.log(response);
			}
		});		
	} else {
		App.view.matrixView.showMatrices.hide();
	}
};

App.controller.matrixController.createEmptyMatrix = function(projectId){
	return {
		"elements" : [],
		"project" : {"id" : projectId}
	};
};

App.controller.matrixController.save = function(matrix){
	Ext.Ajax.request({
		url: App.restServicesBase + '/projects/matrix',
		method: 'POST',
		jsonData: Ext.encode(matrix),
		success: function(response, options){
			alert('Cambios grabados correctamente');
		},
		failure: function(response, options){
			console.log(response);
			alert('Errores al salvar matriz OD');
		}
	});			
};
