Ext.ns('App.controller', 'App.model', 'App.store', 'App.view');

App.geoserverHost = 'http://localhost:8080'; 
App.geoserver = App.geoserverHost + '/geoserver';
App.geoserverRestAuth = 'Basic '+ App.util.Base64.encode('admin:geoserver');

App.restServicesBase = '/restServices/rest';
App.getGeojsonService = '/restServices/main/downjson';
App.postGeojsonService = '/restServices/main/upjson';
App.localWMSStore = App.geoserver + "/wms?service=wms";
App.localWFSStore = App.geoserver + "/wfs?service=wfs";
App.store.capParameters = "&request=getCapabilities&version=1.1.1";
App.localWks = "topp";
App.gsDBStore = "tesisDB";
App.wfsNamespace = "http://www.openplans.org/topp";
App.geometryName = "the_geom";
App.baseLayerMvdLimits = App.localWks + ":depto_p";
App.CRS = 'EPSG:32721';
App.nativeCRS = 'PROJCS[&quot;WGS 84 / UTM zone 21S&quot;, GEOGCS[&quot;WGS 84&quot;, \
    DATUM[&quot;World Geodetic System 1984&quot;,  \
      SPHEROID[&quot;WGS 84&quot;, 6378137.0, 298.257223563, AUTHORITY[&quot;EPSG&quot;,&quot;7030&quot;]],' + 
      'AUTHORITY[&quot;EPSG&quot;,&quot;6326&quot;]], PRIMEM[&quot;Greenwich&quot;, 0.0, AUTHORITY[&quot;EPSG&quot;,&quot;8901&quot;]],' + 
    'UNIT[&quot;degree&quot;, 0.017453292519943295], AXIS[&quot;Geodetic longitude&quot;, EAST],AXIS[&quot;Geodetic latitude&quot;, NORTH],' + 
    'AUTHORITY[&quot;EPSG&quot;,&quot;4326&quot;]],' +  
  'PROJECTION[&quot;Transverse_Mercator&quot;], PARAMETER[&quot;central_meridian&quot;, -57.0],' + 
  'PARAMETER[&quot;latitude_of_origin&quot;, 0.0], PARAMETER[&quot;scale_factor&quot;, 0.9996],' + 
  'PARAMETER[&quot;false_easting&quot;, 500000.0],PARAMETER[&quot;false_northing&quot;, 10000000.0],' + 
  'UNIT[&quot;m&quot;, 1.0], AXIS[&quot;Easting&quot;, EAST],AXIS[&quot;Northing&quot;, NORTH],AUTHORITY[&quot;EPSG&quot;,&quot;32721&quot;]]';	
App.nativeBoundingBox = '<minx>552743.4150523461</minx> \
	<maxx>588867.1495523461</maxx> \
	<miny>6134516.132456765</miny> \
	<maxy>6159799.036456764</maxy>';
App.latLonBoundingBox = '<minx>-56.42408071266088</minx> \
    <maxx>-56.02699959404317</maxx> \
    <miny>-34.93046251187087</miny> \
    <maxy>-34.69997772659051</maxy>';

OpenLayers.IMAGE_RELOAD_ATTEMPTS = 2;
OpenLayers.DOTS_PER_INCH = 25.4 / 0.28;

App.main = function(){
		Ext.QuickTips.init();
//		OpenLayers.ProxyHost = "/cgi-bin/proxy.cgi?url=";

		App.mapBounds = new OpenLayers.Bounds(
		        552743.4150523461, 6134516.132456765,
		        588867.1495523461, 6159799.036456764
		    );
		App.mapOptions = {
		        maxExtent: App.mapBounds,
		        maxResolution: 141.10833789062508,
		        projection: App.CRS,
		        units: 'm'
		    };
		    
		App.map = new OpenLayers.Map('mapa', App.mapOptions);
		var baseLayer = new OpenLayers.Layer.WMS(
		        "Limites Montevideo", App.geoserver + "/wms",
		        {
		            LAYERS: App.baseLayerMvdLimits,
		            transparent: true,
		            tiled: true,
		            tilesOrigin : App.map.maxExtent.left + ',' + App.map.maxExtent.bottom
		        },
		        {
		            buffer: 0,
		            displayOutsideMaxExtent: true,
		            isBaseLayer: true
//		            yx : {App.CRS : false}
		        } 
		);

		App.view.mainGuiFunction();

        App.controller.logToConsole("Bienvenido!", "Abriendo sistema...");
		App.map.addLayers([baseLayer]);
		App.map.setBaseLayer(baseLayer);
		App.map.setLayerZIndex(baseLayer, 0);
};
