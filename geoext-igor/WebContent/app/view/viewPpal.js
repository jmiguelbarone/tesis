App.view.menuHandler = function(item){
	switch (item.text){
	case 'Abrir':
		App.view.openProject();
		break;
	case 'Nuevo':
		App.view.newProject.np();
		break;		
	case 'Cerrar':
		App.controller.closeProject();
		break;	
	case 'Guardar':
		App.controller.saveProject();
		break;	
	default:
		break;
	}	
};

App.view.toolBarItems = {
		id: "toolbars",
		items : [
		         {
		        	 text : "Modificar paradas", 
		        	 hideWithClosedProject: true,
		        	 enableToggle: true, 
		        	 toggleGroup: 'edit', 
		        	 toggleHandler: function(button, pressed){
		        		 if (pressed){ 
		        			 App.view.busStopEditPanel.activate();
		        		 } else {
		        			 App.view.busStopEditPanel.deactivate();
		        		 }
		        	 }
		         },
		         {	
		        	 text : "Modificar zonas", 
		        	 hideWithClosedProject: true,
		        	 enableToggle: true, 
		        	 toggleGroup: 'edit', 
		        	 toggleHandler: function(button, pressed){
	        		 if (pressed){ 
	        			 App.view.zonesEditPanel.activate();
	        		 } else {
	        			 App.view.zonesEditPanel.deactivate();
	        		 }
		         }
		         },
		         {	
					text : "Modificar caminatas",
					hideWithClosedProject: true,
					enableToggle: true, 
					toggleGroup: 'edit', 
					toggleHandler: function(button, pressed){
	        		 if (pressed){ 
	        			 App.view.walkEditPanel.activate();
	        		 } else {
	        			 App.view.walkEditPanel.deactivate();
	        		 }
	        	 }
		         },
		         {	
						text : "Modificar recorrido",
						hideWithClosedProject: true,
						enableToggle: true, 
						toggleGroup: 'edit', 
						toggleHandler: function(button, pressed){
		        		 if (pressed){ 
		        			 App.view.routeEditPanel.activate();
		        		 } else {
		        			 App.view.routeEditPanel.deactivate();
		        		 }
		        	 }
			         },
		         {	
					text : "Matriz de demanda",
					hideWithClosedProject: true,
					enableToggle: true, 
					toggleGroup: 'edit', 
					toggleHandler: function(button, pressed){
		        		 App.controller.matrixController.show(pressed);
					}
			     }
		]
};

App.view.menuItems = {
		id: "menues",
		items : [{text : 'Proyecto',
   					menu : [
   					        {text : 'Nuevo', handler : App.view.menuHandler, hideWithClosedProject: false},
   					        {text : 'Abrir', handler : App.view.menuHandler, hideWithClosedProject: false},
   					        {text : 'Cerrar', handler : App.view.menuHandler, hideWithClosedProject: true},
   					        {text : 'Guardar', handler : App.view.menuHandler, hideWithClosedProject: true}
   				 ]},
				 {text : 'Edicion',
					menu : [
					        {text : 'Deshacer', handler : App.view.menuHandler, hideWithClosedProject: true},
					        {text : 'Rehacer', handler : App.view.menuHandler, hideWithClosedProject: true}
				 ]},
				 {text : 'Fondo',
					menu : [
					        {text : 'Cargar Fondo', handler : App.view.menuHandler},
					        {text : 'Quitar Fondo', handler : App.view.menuHandler},
					        {text : 'Quitar todos los fondos', handler : App.view.menuHandler}
				 ]},
				 {text : 'Elementos de la red',
					menu : [
				 ]},
				 {text : 'Parametros',
					menu : [
					        {text : 'Configurar', handler : App.view.menuHandler, hideWithClosedProject: true}
				 ]},
				 {text : 'Herramientas',
					menu : [
				 ]}
				]
};

App.view.updateMenues = function(){
	var projectClosed = !App.controller.isProjectLoaded();
	
	var menues = Ext.getCmp('menues').items.items;
	
	for (var i = 0; i < menues.length; i++){
		var menu = menues[i].menu.items.items;
		for (var j = 0; j < menu.length; j++){
			if (menu[j].hideWithClosedProject){
				menu[j].setDisabled(projectClosed ? menu[j].hideWithClosedProject : false);				
			}
		}			
	}

	var toolbars = Ext.getCmp('toolbars').items.items;
	for (var i = 0; i < toolbars.length; i++){
		var button = toolbars[i];
		button.setDisabled(projectClosed ? button.hideWithClosedProject : false);
	}
};

App.view.consoleStore = new Ext.data.JsonStore({
	autoLoad: false,
	remoteSort: true,
	fields: [
	    { name: 'message' },
	    { name: 'project' },
	    { name: 'operation' },
	    { name: 'dateOperation', type: 'date'/*, dateFormat: 'd-m-Y H:i:s'*/ }
	],
	sortInfo: {
		field: 'dateOperation',
		direction: 'DESC'
	}
});
App.view.consoleStore.setDefaultSort('dateOperation', 'DESC');

App.view.consoleGrid = new Ext.grid.GridPanel({
	store: App.view.consoleStore,
	layout: 'fit',
	region: 'south',
	split: true,
	collapsible: true,
	height: 130,
	autoScroll: true,
	columns: [
	          { header: 'Mensaje', dataIndex: 'message', sortable: false, width: 200 },
	          { header: 'Proyecto', dataIndex: 'project', sortable: false, width: 80 },
	          { header: 'Operacion', dataIndex: 'operation', sortable: false, width: 80 },
	          { header: 'Fecha', dataIndex: 'dateOperation', sortable: true, width: 85, renderer: Ext.util.Format.dateRenderer('d-m-Y H:i:s') }
	],
    viewConfig: {
        autoFill: true
   }
});

App.view.mainGuiFunction = function(){
new Ext.Viewport({
	id : "panelPpal",
	title: 'igorTP V3',
	layout: 'border',
	items: [
	        	App.view.consoleGrid
	,{
		xtype: 'treepanel',
		region: 'west',
		enableDD: true,
		root: new GeoExt.tree.LayerContainer({
			text: 'Capas',
	        expanded: true
	    }),
		collapsible: true,
		width: 200,
		autoScroll: true
	},
	{
		xtype: 'gx_mappanel',
		map: App.map,
		tbar: App.view.toolBarItems,
		region: 'center'		
	},
	{
		region :  'north',
		xtype : 'panel',
		tbar : App.view.menuItems    		
	}]
});
App.view.updateMenues();

};
