Ext.ns('App.view.addLayer');

App.view.addLayer.shpFileLoader = new Ext.form.FileUploadField({
   	 id: 'txtShpUpFile',
   	 name: 'shp-path',
     xtype: 'fileuploadfield',
     emptyText: 'Seleccione un SHP (Zip)',
     fieldLabel: 'Archivo',
     buttonText: 'Buscar...',
     width: 300,
     buttonCfg: { /*iconCls: 'upload-icon' */}
});

App.view.addLayer.jsonFileLoader = new Ext.form.FileUploadField({
  	 id: 'txtJsonUpFile',
  	 name: 'json-path',
    xtype: 'fileuploadfield',
    emptyText: 'Seleccione archivo GeoJSon',
    fieldLabel: 'Archivo',
    buttonText: 'Buscar...',
    width: 300,
    buttonCfg: { /*iconCls: 'upload-icon' */}
});

App.view.addLayer.shpPanel = new Ext.FormPanel({
	id: 'shpPanel',
	title : 'Shape',
	frame : true,
	fileUpload : true,
	labelWidth : 60,
	height : 250,
	items : [ App.view.addLayer.shpFileLoader ]	
});

App.view.addLayer.jsonPanel = new Ext.FormPanel({
	id: 'jsonPanel',
	title : 'GeoJSon',
	frame : true,
	fileUpload : true,
	labelWidth : 60,
	height : 250,
	items : [ App.view.addLayer.jsonFileLoader ]	
});

App.view.addLayer.WMS_STORE = App.localWMSStore;
App.view.addLayer.wmsPanel = new Ext.FormPanel({
	title : 'WMS',
	frame : true,
	labelWidth : 80,
	height: 250,
	items : [
	         {
	        	 xtype : 'compositefield',
	        	 fieldLabel : 'Url servidor',
	        	 items : [
	        	          {xtype : 'textfield', name : 'wmsPanelUrl', emptyText : 'Ingrese la URL'}, 
	        	          {xtype : 'button', 
	        	        	  text : '...',
	        	        	  handler : function (){
	     		        		 var urlField = App.view.addLayer.wmsPanel.getForm().findField('wmsPanelUrl');
	     		        		 var wmsValue = urlField.getValue();
	     		        		 
	     		        		 if (wmsValue){
	     		        			App.view.addLayer.WMS_STORE = wmsValue;
	     		        			App.store.capStore.url = /* App.store.proxy + */ App.view.addLayer.WMS_STORE + App.store.capParameters;
	     		        		 }

     		        			App.store.capStore.load();
     		        			App.view.addLayer.wmsPanel.doLayout();
	     		        	  }}
	        	          ]
	         },
	         {
	        	 xtype : 'grid',
	        	 id : 'wmsPanelGrid',
	        	 frame : true,
	        	 width: 470,
	        	 height: 250,
	             columns: [
	                       {header: "Titulo", dataIndex: "title", sortable : false, width: 25},
	                       {header: "Nombre", dataIndex: "name", sortable :  false, width: 35},
	                       {header: "Consultable", dataIndex: "queryable", sortable : false, width: 20},
	                       {id: "description", header: "Descripcion", dataIndex: "abstract", width: 20}
                 ],
                 store : App.store.capStore,
                 viewConfig: {
                     autoFill: true
                }
	         }
	]
});

App.view.addLayer.addLayerFormTabs = new Ext.TabPanel({
	activeTab : 0,
	items : [App.view.addLayer.shpPanel, App.view.addLayer.jsonPanel, App.view.addLayer.wmsPanel]
});

App.view.addLayer.addLayerComponent = Ext.extend(Ext.Window, {
	constructor: function(config){
		this.addEvents({'addLayer': true});
		App.view.addLayer.addLayerComponent.superclass.constructor.call(this, config);
	},

	initComponent: function(){
		Ext.apply(this, {
			closable : true,
			border : true,
			width : 500,
			height : 400,
			layout: 'vbox',
			items : [App.view.addLayer.addLayerFormTabs, 
			         { xtype: 'panel',
					   layout: 'fit',
					   items: [{xtype: 'checkbox', 
						   		id: 'chkIsLayerBase',
						   		checked: false, 
						   		boxLabel: 'Capa base (fondo)?',
						   		boxLabelAlign: 'before'}]
			         }],
			buttonAlign : 'right',
			buttons : [
			           {text: 'Ok', scope: this, handler : function(){
					 	   this.hide();

							switch (App.view.addLayer.addLayerFormTabs.getActiveTab()){
							case App.view.addLayer.shpPanel:
								var fileContent = App.view.addLayer.shpFileLoader.fileInput.dom.files[0]; // Objeto file a cargar
								var fileName = App.view.addLayer.shpFileLoader.getValue().substring(0, len - 4); // Nombre sin .zip
								this.fireEvent('addLayer', {"title": fileName, "store": fileContent, "name": fileName, "baseLayer": Ext.getCmp('chkIsLayerBase').getValue(), "type": "SHP"});
								break;
							case App.view.addLayer.jsonPanel:
								var fileContent = App.view.addLayer.jsonFileLoader.fileInput.dom.files[0]; // Objeto file a cargar
								var fileName = App.view.addLayer.jsonFileLoader.getValue();
								this.fireEvent('addLayer', {"title": fileName, "store": fileContent, "name": fileName, "baseLayer": Ext.getCmp('chkIsLayerBase').getValue(), "type": "JSON"});
								break;
							case App.view.addLayer.wmsPanel:
								var grid = Ext.getCmp('wmsPanelGrid');
								var record = grid.getSelectionModel().getSelected();
								
								this.fireEvent('addLayer', {"title": record.get('title'), "store": App.view.addLayer.WMS_STORE, "name":record.get('name'), "baseLayer": Ext.getCmp('chkIsLayerBase').getValue(), "type": "WMS"});
								break;
							default:
								log('Tab ' + App.view.addLayer.addLayerFormTabs.getActiveTab());
								break;
							}	        				        	   
					   }}, 
			           {text: 'Cancelar', scope: this, handler : function(){
					 	   this.hide(); 		
						}}
			          ]
			});
		App.view.addLayer.addLayerComponent.superclass.initComponent.apply(this, arguments);		
	}
});
