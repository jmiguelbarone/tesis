Ext.ns("App.view.newProject");

App.view.newProject.demandLayer = {};
App.view.newProject.roadLayer = {};

App.view.newProject.itemsFirstCard = [ {
	name : "name",
	allowBlank: false,
	xtype : "textfield",
	fieldLabel : "Nombre"
}, {
	name : "description",
	allowBlank: false,
	xtype : "textfield",
	fieldLabel : "Descripcion"
} ];
App.view.newProject.itemsSecondCard = [ {
	xtype : "compositefield",
	fieldLabel : "Elegir capa",
	items : [ {
		id: "txtCapaDemanda",
		name : "txtCapaDemanda",
		xtype : "textfield",
		allowBlank: false,
		emptyText : "Busque la capa"
	}, {
		id : "butCapaDemanda",
		xtype : "button",
		text : "...",
		handler : function(){ 
			App.view.addLayer.getLayer({
				addToProyect: false,
				success: function(layer){
					Ext.getCmp("txtCapaDemanda").setValue(layer.title);
					App.view.newProject.demandLayer = layer;
					console.log("demanda: " + layer);
				}
			}); 
		}
	} ]
}, {
	name : "cmbIdDemanda",
	xtype : "combo",
	allowBlank: true,
	fieldLabel : "Identificador"
} ];
App.view.newProject.itemsThirdCard = [ {
	xtype : "compositefield",
	fieldLabel : "Elegir capa",
	items : [ {
		id: "txtCapaVias",
		name : "txtCapaVias",
		allowBlank: false,
		xtype : "textfield",
		emptyText : "Busque la capa"
	}, {
		id : "butCapaVias",
		xtype : "button",
		text : "...",
		handler : function(){
			addToProyect: false,
			App.view.addLayer.getLayer({
				addToProyect: false,
				success: function(layer){
					Ext.getCmp("txtCapaVias").setValue(layer.title);
					App.view.newProject.roadLayer = layer;
					console.log("vias: " + layer);
				}
			});
		}
	}]
} ];

App.view.newProject.itemsFourthCard = [ {
	name : "pedestrianSpeed",
	xtype : "textfield",
	allowBlank: false,
	fieldLabel : "Velocidad peaton"
}, {
	name : "floatSpeed",
	allowBlank: false,
	xtype : "textfield",
	fieldLabel : "Velocidad flota"
} ];

App.view.newProject.items = [ {
	id : 'firstPanel',
	xtype : "form",
	title : "Proyecto",
	border : true,
	items : App.view.newProject.itemsFirstCard
}, {
	id : 'secondPanel',
	xtype : "form",
	title : "Puntos de demanda",
	border : true,
	items : App.view.newProject.itemsSecondCard
}, {
	id : 'thirdPanel',
	xtype : "form",
	title : "Vias",
	border : true,
	items : App.view.newProject.itemsThirdCard
}, {
	id : 'fourthPanel',
	xtype : "form",
	title : "Generales",
	border : true,
	items : App.view.newProject.itemsFourthCard
} ];

App.view.newProject.np = function() {
	var index = 0;

	var next = function() {
		backBut.show();
		if (index < App.view.newProject.items.length - 1) {
			index++;
			var cardlayout = win.getLayout();
			cardlayout.setActiveItem(index);

			if (index == App.view.newProject.items.length - 1) { // si esta
																	// en el
																	// ultima
																	// carta
				nextBut.hide();
				finishBut.show();
			}
		}
	};
	var back = function() {
		if (index > 0) {
			index--;
			var cardlayout = win.getLayout();
			cardlayout.setActiveItem(index);
		}

		nextBut.show();
		if (index == 0) { // si esta en la primera carta
			backBut.hide();
			finishBut.hide();
		}
	};
	var finish = function() {
		var firstForm = win.items.items[0].getForm();
		var secondForm = win.items.items[1].getForm();
		var thirdForm = win.items.items[2].getForm();
		var fourthForm = win.items.items[3].getForm();
		
		if (!firstForm.isValid()){
			alert("1er etapa invalida");
			return;
		}
		if (!secondForm.isValid()){
			alert("2da etapa invalida");
			return;			
		}
		if (!thirdForm.isValid()){
			alert("3a etapa invalida");
			return;			
		}
		if (!fourthForm.isValid()){
			alert("4ta etapa invalida");
			return;			
		}
		
		
		var firstFormValues = firstForm.getValues();
		var secondFormValues = secondForm.getValues();
		var thirdFormValues = thirdForm.getValues();
		var fourthFormValues = fourthForm.getValues();

		var formData = '{"name": "' + firstFormValues.name + '"' +
		', "description": "' + firstFormValues.description + '"' +
		', "pedestrianSpeed": ' + fourthFormValues.pedestrianSpeed + 
		', "floatSpeed": ' + fourthFormValues.floatSpeed +
		', "distance": "EUCLIDEAN"' +
		', "layers": []' +
		', "demandPointsLayer": ' + Ext.encode(App.view.newProject.demandLayer) +
		', "roadLayer": ' + Ext.encode(App.view.newProject.roadLayer) +
		'}';

		App.controller.newProject(Ext.decode(formData));
		win.close();
	};

	var backBut = new Ext.Button({
		text : "Atras",
		handler : back,
		scope : this,
		hidden : true
	});
	var nextBut = new Ext.Button({
		text : "Adelante",
		handler : next,
		scope : this
	});
	var finishBut = new Ext.Button({
		text : "Fin",
		handler : finish,
		scope : this,
		hidden : true
	});

	var win = new Ext.Window({
		title : "Nuevo proyecto...",
		layout : "card",
		width : 450,
		height : 400,
		resizable : false,
		fbar : [ backBut, nextBut, finishBut ],
		// items: form
		items : App.view.newProject.items
	});

	win.show();
	win.getLayout().setActiveItem(index);
};
