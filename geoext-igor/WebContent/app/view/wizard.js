App.view.Wizard = Ext.extend(Object, {
	index: 0,
	constructor: function(windowTitle, wizardItems, parUrl){
		this.urlForm = parUrl;
		this.form = new Ext.FormPanel({
			layout: "card",
			border: false,
			activeItem: this.index,
			items: wizardItems,
			success	: Ext.Msg.alert("Alert","Your connection has been created succesfully."),
			failure	: Ext.Msg.alert("Alert","Error!!!")			
		});
		
		this.backBut = new Ext.Button({text: "Atras", handler: this.back, scope: this, hidden: true});
		this.nextBut = new Ext.Button({text: "Adelante", handler: this.next, scope: this});
		this.finishBut = new Ext.Button({text: "Fin", handler: this.finish, scope: this, hidden: true});
	
		this.win = new Ext.Window({
			title: windowTitle,
			layout: "fit",
			width: 450,
			height: 400,
			resizable: false,
			fbar: [this.backBut, this.nextBut, this.finishBut],
			items: this.form
		});
		
		this.win.show();
	},
	next: function(){
		this.backBut.show();
		if(this.index < this.form.items.length-1){
			this.index++;
			var cardlayout = this.form.getLayout();
			cardlayout.setActiveItem(this.index);
			
			if(this.index == this.form.items.length-1){	//si esta en el ultima carta
				this.nextBut.hide();
				this.finishBut.show();
			}
		}		
	},
	back: function(){
		if(this.index>0){
			this.index--;
			var cardlayout = this.form.getLayout();
			cardlayout.setActiveItem(this.index);
		}
		
		if(this.index == 0){	//si esta en la primera carta
			this.backBut.hide();	
		}else{
			this.finishBut.hide();
			this.nextBut.show();
		}		
	},
	finish: function(){
        var formData = Ext.encode(this.form.getForm().getValues());	
		
		Ext.Ajax.request({
            url : this.urlForm,
            method: 'POST',
            waitTitle: 'Conectando...',
            waitMsg: 'Enviando datos...',
            jsonData: formData
        });
		this.win.close();
	}
});
