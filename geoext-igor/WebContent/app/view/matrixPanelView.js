Ext.ns('App.view.matrixView');

App.view.matrixView.matrixStore = new Ext.data.JsonStore({
	fields: [{name: 'id', type: Ext.data.Types.INT}, {name: 'beginCentroid', type: Ext.data.Types.INT}, {name: 'endCentroid', type: Ext.data.Types.INT}],
	autoLoad: false
});

App.view.matrixView.matricesForm = new Ext.FormPanel({
	items : [
	         {
	        	 xtype : 'grid',
	        	 id : 'matrixFormGrid',
	        	 width: 280,
	        	 height: 150,
	             columns: [
	                       {header: "Centroide origen", dataIndex: "beginCentroid", sortable : false, width: 120},
	                       {header: "Centroide destino", dataIndex: "endCentroid", sortable : false, width: 120}
                 ],
                 store : App.view.matrixView.matrixStore,
                 viewConfig: {
                     autoFill: false
                },
                listeners: {
                	'rowclick': function(grid,rowIndex,cellIndex,e){
                		var tbbar = Ext.getCmp('odMatrixToolbar');
                		
                		if (grid.getSelectionModel().getCount() > 0){
                			tbbar.enable();
                		} else {
                			tbbar.disable();
                			Ext.getCmp('odMatrixInsertButton').enable();
                		}
                	}
                }
	         }
	]	
});

App.view.matrixView.editODHandler = function(button){
	switch (button.id){
	case 'odMatrixButton1':
		var control = App.controller.getSelectControl(App.model.project.centroidsLayer);
		control.events.register('featurehighlighted', undefined, App.view.matrixView.zoneSelectBegin);
		control.activate();
		break;
	case 'odMatrixButton2':
		var control = App.controller.getSelectControl(App.model.project.centroidsLayer);
		control.events.register('featurehighlighted', undefined, App.view.matrixView.zoneSelectEnd);
		control.activate();
		break;
	case 'odMatrixDeleteButton':
		var record = Ext.getCmp('matrixFormGrid').getSelectionModel().getSelected();
		App.view.matrixView.matrixStore.remove(record);
		break;
	case 'odMatrixInsertButton':
		var record = new App.view.matrixView.matrixStore.recordType({'beginCentroid': 0, 'endCentroid': 0});
		App.view.matrixView.matrixStore.add(record);
		Ext.getCmp('matrixFormGrid').getSelectionModel().selectLastRow();
		
		break;
	default:
			break;
	};
		
};

App.view.matrixView.zoneSelectBegin = function(zone){
	var control = App.controller.getSelectControl(App.model.project.centroidsLayer);
	control.events.unregister('featurehighlighted', undefined, App.view.matrixView.zoneSelectBegin);

	var grid = Ext.getCmp('matrixFormGrid');
	
	var record = grid.getSelectionModel().getSelected();
	record.data.beginCentroid = Number(zone.feature.fid.split('.')[1]); 
	grid.getView().refresh();

	control.deactivate();
};

App.view.matrixView.zoneSelectEnd = function(zone){
	var control = App.controller.getSelectControl(App.model.project.centroidsLayer);
	control.deactivate();
	control.events.unregister('featurehighlighted', undefined, App.view.matrixView.zoneSelectBegin);
	var grid = Ext.getCmp('matrixFormGrid');
	
	var record = grid.getSelectionModel().getSelected();
	record.data.endCentroid = Number(zone.feature.fid.split('.')[1]); 
	grid.getView().refresh();
};

App.view.matrixView.showMatrixClass = Ext.extend(Ext.Window, {
	matrix : undefined,
	closable : true,
	border : true,
	width : 300,
	height : 250,
	layout: 'vbox',
	tbar: {
		xtype: 'toolbar',
		id: 'odMatrixToolbar',
		items: [
	    {id: 'odMatrixButton1', disabled: true, text: 'Editar origen', handler: App.view.matrixView.editODHandler},
	    {id: 'odMatrixButton2', disabled: true, text: 'Editar destino', handler: App.view.matrixView.editODHandler},
	    {id: 'odMatrixDeleteButton', disabled: true, text: 'Eliminar', handler: App.view.matrixView.editODHandler},
	    {id: 'odMatrixInsertButton', disabled: false, text: 'Insertar', handler: App.view.matrixView.editODHandler}
	]},
	buttonAlign : 'right',
	buttons : [ {text: 'Ok'}, {text: 'Cancelar'} ],
	initComponent: function(){
		App.view.matrixView.showMatrixClass.superclass.initComponent.call(this);
		this.buttons[0].on('click', this.buttonsHandler, this);
		this.buttons[1].on('click', this.buttonsHandler, this);
	},
	showMatrix : function(matrix){
		this.matrix = matrix;
		this.title = 'Matriz OD del proyecto ' + matrix.project.name;
		App.view.matrixView.matrixStore.loadData(matrix.elements);
		this.show();
	},
	buttonsHandler: function(button){
		this.hide();
		if (button.text == 'Ok'){
			App.view.matrixView.matrixStore.commitChanges();
			var recs = App.view.matrixView.matrixStore.getRange();
			this.matrix.elements.length = 0;
			for (var i = 0; i < recs.length; i++){
				this.matrix.elements.push(recs[i].data);
			}
			App.controller.matrixController.save(this.matrix);
		}
	}
});

App.view.matrixView.showMatrices = new App.view.matrixView.showMatrixClass({
	items : {
   	 xtype : 'grid',
   	 id : 'matrixFormGrid',
   	 width: 280,
   	 height: 150,
     columns: [
         {header: "Centroide origen", dataIndex: "beginCentroid", sortable : false, width: 120},
         {header: "Centroide destino", dataIndex: "endCentroid", sortable : false, width: 120}
     ],
     store : App.view.matrixView.matrixStore, 
     viewConfig: {autoFill: false},
     listeners: {
       	 'rowclick': function(grid,rowIndex,cellIndex,e){
       		var tbbar = Ext.getCmp('odMatrixToolbar');
              		
       		if (grid.getSelectionModel().getCount() > 0){
       			tbbar.enable();
       		} else {
              	tbbar.disable();
              	Ext.getCmp('odMatrixInsertButton').enable();
            }
       	 }
     }
    }
});

