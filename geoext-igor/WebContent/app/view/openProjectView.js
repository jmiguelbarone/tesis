
App.view.openProjectStore = new Ext.data.JsonStore({
	root: 'projects',
	fields: [{name: 'id', type: Ext.data.Types.INT}, {name: 'name', type: 'string'}, {name: 'description', type: 'string'}],
	autoLoad: false,
	proxy: new Ext.data.HttpProxy({
		url: App.restServicesBase + '/projects/list.json',
		method: 'POST'
	})
});

App.view.openProjectStore.on('exception', function(proxy, type, action, options, response, args){
	console.log(proxy, type, action, options, response, args);
});

App.view.openProjectForm = new Ext.FormPanel({
	title : 'Abrir proyecto...',
	items : [
	         {
	        	 xtype : 'grid',
	        	 id : 'openProjectGrid',
	        	 width: 400,
	        	 height: 150,
	             columns: [
	                       {header: "Nombre", dataIndex: "name", sortable : true, width: 100},
	                       {header: "Descripcion", dataIndex: "description", sortable : false, width: 300}
                 ],
                 store : App.view.openProjectStore,
                 viewConfig: {
                     autoFill: true
                }
	         }
	]	
});

App.view.openProjectWindow = new Ext.Window({
	closable : true,
	border : true,
	width : 400,
	height : 200,
	layout: 'vbox',
	items : [App.view.openProjectForm],
	buttonAlign : 'right',
	buttons : [
	           {text: 'Ok',
	        	handler : function(){
	        		App.view.openProjectWindow.hide();
	        		var grid = Ext.getCmp('openProjectGrid');
	        		var record = grid.getSelectionModel().getSelected();
        			App.controller.loadProject(record.data.id, record.data.name);
	        	} 
	           }, 
	           {text: 'Cancelar',
	        	handler : function (){ App.view.openProjectWindow.hide(); }
	           }
	]
});

App.view.openProject = function(){
	App.view.openProjectStore.load();
	App.view.openProjectWindow.show();
};
