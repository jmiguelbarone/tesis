package uy.edu.fing.igortp.services.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import uy.edu.fing.igortp.data.ODElement;
import uy.edu.fing.igortp.data.ODMatrix;
import uy.edu.fing.igortp.data.Project;
import uy.edu.fing.igortp.data.ProjectList;
import uy.edu.fing.igortp.data.dao.GenericDao;

@Path("projects")
public class ProjectServices {	
	private GenericDao<Project> dao = new GenericDao<Project>(Project.class);
	private GenericDao<ODMatrix> matrixDao = new GenericDao<ODMatrix>(ODMatrix.class);

	@GET
	@Path("index")
	@Produces("text/html")
	public Response showServices(){
		return Response.status(200).entity("Uaajaaa ... project services!!!").build();
	}
	
	@POST
	@Path("list")
	@Produces("application/json")
	public ProjectList getProjectsJsonPost(){
		System.out.println("Projects en POST/json");
		return getProjects();
	}

	@GET
	@Path("list")
	@Produces("application/json")
	public ProjectList getProjectsJsonGet(){
		System.out.println("Projects en GET/json");
		return getProjectsJsonPost();
	}

	@GET
	@Path("list")
	@Produces("application/xml")
	public ProjectList getProjectsXml(){
		System.out.println("Projects en GET/xml");
		return getProjects();
	}
	
	@POST
	@Path("save")
	@Consumes("application/json")
	public Response saveProject(Project proj){
		if (proj == null){
			return Response.status(500).entity("El proyecto no puede ser nulo...").build();
		}
		
		if (proj.getId() != null){
			Project tmp = dao.getEntity(proj.getId());
			tmp.setLayers(proj.getLayers());
			dao.updateEntity(tmp);
		} else {
			dao.saveEntity(proj);
		}
		
		return Response.status(200).entity(proj.toString()).build();
	}

	@DELETE
	@Path("{id}")
	public Response deleteProject(@PathParam("id") Long id){
		dao.deleteEntity(id);
		return Response.status(200).entity("OK").build();
	}

	@GET
	@Path("{id}")
	@Produces("application/json")
	public Project getProject(@PathParam("id") Long id){
		return dao.getEntity(id);
	}

	private ProjectList getProjects(){	
		List<Project> projects = dao.getEntities("id desc");
		ProjectList projectList = new ProjectList(projects);
		
		return projectList;
	}
	
	@GET
	@Path("matrix/{project}")
	@Produces("application/json")
	public ODMatrix getProjectODMatrix(@PathParam("project") Long id){
		List<ODMatrix> matrices = matrixDao.getEntities("project.id", id);
		
		if (matrices.isEmpty()) return null;
		
		ODMatrix matrix =  matrices.get(0);
/*
		EntityManager em = SessionManager.getInstance().getSessionFactory().createEntityManager();
		em.getTransaction().begin();
		matrix.getProject();
		matrix.getElements();
		em.getTransaction().commit();
		em.close();
*/		
		return matrix;
	}	

	@POST
	@Path("matrix")
	@Consumes("application/json")
	public Response saveMatrix(ODMatrix matrix){
		if (matrix == null){
			return Response.status(500).entity("La matriz OD  no puede ser nula...").build();
		}
		
		if (matrix.getId() != null){
			ODMatrix tmp = matrixDao.getEntity(matrix.getId());
			for (ODElement element : matrix.getElements()){
				element.setMatrix(matrix);
			}
			tmp.setElements(matrix.getElements());
			matrixDao.updateEntity(tmp);
		} else {
			matrixDao.saveEntity(matrix);
		}
		
		return Response.status(200).entity(matrix.toString()).build();
	}

}
