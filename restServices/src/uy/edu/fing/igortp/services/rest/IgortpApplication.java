package uy.edu.fing.igortp.services.rest;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

public class IgortpApplication extends Application {
	   private Set<Object> singletons = new HashSet<Object>();
	   private Set<Class<?>> empty = new HashSet<Class<?>>();

	   public IgortpApplication() {
	      singletons.add(new ProjectServices());
	      singletons.add(new MainServices());
	      singletons.add(new GraphServices());
	   }

	   @Override
	   public Set<Class<?>> getClasses() {
	      return empty;
	   }

	   @Override
	   public Set<Object> getSingletons() {
	      return singletons;
	   }
}
