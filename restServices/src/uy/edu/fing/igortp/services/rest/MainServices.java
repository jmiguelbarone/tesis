package uy.edu.fing.igortp.services.rest;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

@Path("main")
public class MainServices {
	public static final String SHAPEFILES_PATH = "/home/jmiguel/workspaces/tesis/geoserver-data/data/shapefiles/";
	public static final String JSON_FILES_PATH = "jsonFilesPath";
	
	@GET
	@Path("index")
	@Produces("text/html")
	public Response showServices() {
		return Response.status(200).entity("Uaajaaa ... main services!!!")
				.build();
	}

	@POST
	@Path("upjson")
	@Produces("application/json")
	public Response upJson(@QueryParam("name") String name, 
			@QueryParam("project") String project, 
			@Context HttpServletRequest request) throws Exception {
		ServletContext context = request.getServletContext();
		String basePath = context.getInitParameter(JSON_FILES_PATH);
		String path = project + "/" + name;
		
		String fileName = basePath == null ? context.getRealPath(path) : basePath + File.separator + path;
		
		DataInputStream dis = new DataInputStream(request.getInputStream());
		System.out.println("Guardando: " + fileName);
		File file = new File(fileName);
		if (!file.exists()) {
			file.getParentFile().mkdirs();
			file.createNewFile();
		}

		FileOutputStream fos = new FileOutputStream(file);
		byte[] buffer = new byte[10240];
		int len;

		try {
			while ((len = dis.read(buffer)) != -1) {
				fos.write(buffer, 0, len);
			}
		} finally {
			if (fos != null) {
				fos.flush();
				fos.close();
			}
		}
		
		return Response.status(200).entity(project + "/" + name).build();
	}

	@GET
	@Path("downjson")
	@Produces("application/json")
	public Response downJson(@QueryParam("name") String name, 
			@QueryParam("project") String project, 
			@Context HttpServletRequest request) throws Exception {
		ServletContext context = request.getServletContext();
		String basePath = context.getInitParameter(JSON_FILES_PATH);
		String path = project + "/" + name;
		
		String fileName = basePath == null ? context.getRealPath(path) : basePath + File.separator + path;
		System.out.println("Devolviendo: " + fileName);
		File file = new File(fileName);
		if (!file.exists()){
			return Response.status(404).build();
		}
		
		ResponseBuilder response = Response.ok(file);
		response.header("Content-Disposition",
				"attachment; filename=" + name + ".json");
		return response.build();
	}
	
	@POST
	@Path("upshp")
	@Produces("text/plain")
	public Response upShapeFile(@QueryParam("name") String name,
			@Context HttpServletRequest request) throws Exception {
		String fileName = SHAPEFILES_PATH + name + File.separator + name
				+ ".zip";

		DataInputStream dis = new DataInputStream(request.getInputStream());
		System.out.println("Guardando: " + fileName);
		File file = new File(fileName);
		if (!file.exists()) {
			file.getParentFile().mkdirs();
			file.createNewFile();
		}

		FileOutputStream fos = new FileOutputStream(file);
		byte[] buffer = new byte[10240];
		int len;

		try {
			while ((len = dis.read(buffer)) != -1) {
				fos.write(buffer, 0, len);
			}
		} finally {
			if (fos != null) {
				fos.flush();
				fos.close();
				unzip(file);
				System.out.println("Eliminando zip");
				file.delete();
			}
		}

		return Response.status(200).entity("file:" + file.getAbsolutePath())
				.build();
	}

	private void unzip(File file) throws Exception {
		byte[] buffer = new byte[10240];
		ZipInputStream zis = new ZipInputStream(new FileInputStream(file));

		ZipEntry ze = zis.getNextEntry();
		while (ze != null) {
			System.out.println("Descomprimiendo " + ze.getName());
			File newFile = new File(file.getParent() + File.separator
					+ ze.getName());
			FileOutputStream fos = new FileOutputStream(newFile);

			int len;
			while ((len = zis.read(buffer)) > 0) {
				fos.write(buffer, 0, len);
			}

			fos.close();
			ze = zis.getNextEntry();
		}
		
		zis.closeEntry();
		zis.close();

	}
}
