package uy.edu.fing.igortp.services.rest;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import uy.edu.fing.igortp.data.Layer;
import uy.edu.fing.igortp.data.Project;
import uy.edu.fing.igortp.data.dao.GenericDao;
import uy.edu.fing.igortp.data.dao.SessionManager;
import uy.edu.fing.igortp.data.graph.Graph;

@Path("graph")
public class GraphServices {	
	private GenericDao<Graph> dao = new GenericDao<Graph>(Graph.class);
	private GenericDao<Project> projectDao = new GenericDao<Project>(Project.class);

	@GET
	@Path("index")
	@Produces("text/html")
	public Response showServices(){
		return Response.status(200).entity("Uaajaaa ... graph services!!!").build();
	}
	
	@POST
	@Path("save")
	@Consumes("application/json")
	public Response saveGraph(Graph graph){
		if (graph == null){
			return Response.status(500).entity("El grafo no puede ser nulo...").build();
		}
		
		if (graph.getId() != null){
			Graph tmp = dao.getEntity(graph.getId());
			dao.updateEntity(tmp);
		} else {
			dao.saveEntity(graph);
		}
		
		return Response.status(200).build();
	}

	@DELETE
	@Path("{id}")
	public Response deleteGraph(@PathParam("id") Long id){
		dao.deleteEntity(id);
		return Response.status(200).entity("OK").build();
	}

	@GET
	@Path("{id}")
	@Produces("application/json")
	public Graph getGraph(@PathParam("id") Long id){
		return dao.getEntity(id);
	}

	@GET
	@Path("project/{project}")
	@Produces("application/json")
	public Graph getProjectGraph(@PathParam("project") Long id){
		List<Graph> graphList = dao.getEntities("project.id", id);
		
		if (graphList.isEmpty()) return null;
		
		Graph graph =  graphList.get(0);

		return graph;
	}	
	
	@GET
	@Path("create/{project}")
	public Response createGraph(@PathParam("project") Long id){
		Project project = projectDao.getEntity(id);
		Layer centroidsLayer = project.getCentroidsLayer();
		
		EntityManager em = SessionManager.getInstance().getSessionFactory().createEntityManager();
		em.getTransaction().begin();

		Query q = em.createNativeQuery("select fid, ST_AsText(the_geom) from " + centroidsLayer.getName());
		List<Object[]> result = q.getResultList();
		
		em.getTransaction().commit();
		em.close();

		Graph g = new Graph();
		for (Object[] o : result){
			System.out.println("Centroide: " + o[0] + " - " + o[1]);
		
		}
		
		return Response.status(200).entity("Ok").build();		
	}
}
