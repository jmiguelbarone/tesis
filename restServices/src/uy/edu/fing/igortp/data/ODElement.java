package uy.edu.fing.igortp.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnore;

@XmlRootElement(name="od_element")
@Entity
public class ODElement {
	@Id
	@GeneratedValue
	private Long id;
	@ManyToOne
	private ODMatrix matrix;
	private Long beginCentroid;
	private Long endCentroid;

	public Long getId() {
		return id;
	}

	@XmlElement
	public void setId(Long id) {
		this.id = id;
	}

	public Long getBeginCentroid() {
		return beginCentroid;
	}

	@XmlElement
	public void setBeginCentroid(Long beginCentroid) {
		this.beginCentroid = beginCentroid;
	}
	
	public Long getEndCentroid() {
		return endCentroid;
	}
	
	@XmlElement
	public void setEndCentroid(Long endCentroid) {
		this.endCentroid = endCentroid;
	}

	@JsonIgnore
	public ODMatrix getMatrix() {
		return matrix;
	}

	public void setMatrix(ODMatrix matrix) {
		this.matrix = matrix;
	}
}
