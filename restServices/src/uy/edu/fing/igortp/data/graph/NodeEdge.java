package uy.edu.fing.igortp.data.graph;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class NodeEdge {

	@Id
	@GeneratedValue
	private Long id;
	@OneToOne
	private Node node;
	@OneToOne
	private Edge edge;
	
	public NodeEdge(){};
	
	public NodeEdge(Node node, Edge edge){
		setNode(node);
		setEdge(edge);
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Node getNode() {
		return node;
	}
	public void setNode(Node node) {
		this.node = node;
	}
	public Edge getEdge() {
		return edge;
	}
	public void setEdge(Edge edge) {
		this.edge = edge;
	}
}
