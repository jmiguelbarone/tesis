package uy.edu.fing.igortp.data.graph;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import uy.edu.fing.igortp.data.Project;

@Entity
public class Graph {
	@Id
	@GeneratedValue
	private Long id;
	@OneToOne
	private Project project;
	@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL, orphanRemoval=true)
	private Map<Node, NodeEdge> vertices;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
	}
	public Map<Node, NodeEdge> getVertices() {
		return vertices;
	}
	public void setVertices(Map<Node, NodeEdge> vertices) {
		this.vertices = vertices;
	}
	
	public void addNode(Node node, NodeEdge edge){
		if (this.vertices == null){
			this.vertices = new HashMap<Node, NodeEdge>();
		}
		
		this.vertices.put(node, edge);
	}
}
