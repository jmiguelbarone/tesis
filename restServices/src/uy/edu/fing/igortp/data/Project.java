package uy.edu.fing.igortp.data;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="project")
@Entity
public class Project {
	@Id
	@GeneratedValue
	private Long id;
	private String name;
	private String description;
	@OneToMany (fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	private List<Layer> layers = new ArrayList<Layer>(); 
	private Integer pedestrianSpeed; 
	private Integer floatSpeed;
	public static enum Distance {EUCLIDEAN, MANHATTAN};
	private Distance distance = Distance.EUCLIDEAN;
	@OneToOne (fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	private Layer busStopLayer;
	@OneToOne (fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	private Layer roadLayer;
	@OneToOne (fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	private Layer centroidsLayer;
	@OneToOne (fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	private Layer demandPointsLayer;
	@OneToOne (fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	private Layer zonesLayer;
	@OneToOne (fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	private Layer walkLayer;
	@OneToOne (fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	private Layer routeLayer;
	
	public Project(){}

	public Project(String name, String desc, int pedestrianSpeed, Layer dm){
		setName(name);
		setDescription(desc);
//		setDemandPoints(dm);
		setPedestrianSpeed(pedestrianSpeed);
	}
	
	public Long getId() {
		return id;
	}

	@XmlElement
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	@XmlElement
	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	@XmlElement
	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getPedestrianSpeed() {
		return pedestrianSpeed;
	}

	@XmlElement
	public void setPedestrianSpeed(Integer pedestrianSpeed) {
		this.pedestrianSpeed = pedestrianSpeed;
	}

	public Distance getDistance() {
		return distance;
	}

	@XmlElement
	public void setDistance(Distance distance) {
		this.distance = distance;
	}

	public List<Layer> getLayers() {
		return layers;
	}

	@XmlElement
	public void setLayers(List<Layer> layers) {
		this.layers = layers;
	}


	public Integer getFloatSpeed() {
		return floatSpeed;
	}

	@XmlElement
	public void setFloatSpeed(Integer floatSpeed) {
		this.floatSpeed = floatSpeed;
	}
	
	public Layer getBusStopLayer() {
		return busStopLayer;
	}

	@XmlElement
	public void setBusStopLayer(Layer busStopLayer) {
		this.busStopLayer = busStopLayer;
	}

	public Layer getRoadLayer() {
		return roadLayer;
	}

	@XmlElement
	public void setRoadLayer(Layer roadLayer) {
		this.roadLayer = roadLayer;
	}

	public Layer getCentroidsLayer() {
		return centroidsLayer;
	}

	@XmlElement
	public void setCentroidsLayer(Layer centroidsLayer) {
		this.centroidsLayer = centroidsLayer;
	}

	public Layer getDemandPointsLayer() {
		return demandPointsLayer;
	}

	@XmlElement
	public void setDemandPointsLayer(Layer demandPointsLayer) {
		this.demandPointsLayer = demandPointsLayer;
	}

	public Layer getZonesLayer() {
		return zonesLayer;
	}

	@XmlElement
	public void setZonesLayer(Layer zonesLayer) {
		this.zonesLayer = zonesLayer;
	}

	public Layer getRouteLayer() {
		return routeLayer;
	}

	@XmlElement
	public void setRouteLayer(Layer routeLayer) {
		this.routeLayer = routeLayer;
	}

	public Layer getWalkLayer() {
		return walkLayer;
	}

	@XmlElement
	public void setWalkLayer(Layer walkLayer) {
		this.walkLayer = walkLayer;
	}

	@Override
	public String toString() {
		return "Project [id=" + id + ", name=" + name + ", description="
				+ description + "]";
	}	
}
