package uy.edu.fing.igortp.data;

import java.util.Collection;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="projects")
public class ProjectList {
	private Collection<Project> projects;
	
	public ProjectList(){}
	
	public ProjectList(Collection<Project> l){
		projects = l;
	}

	@XmlElement (name="project")
	public Collection<Project> getProjects() {
		return projects;
	}
}
