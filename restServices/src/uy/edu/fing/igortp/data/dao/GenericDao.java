package uy.edu.fing.igortp.data.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;


public class GenericDao<T> {
	private EntityManagerFactory entityManagerFactory;
	private Class<T> clazz;
	
	public GenericDao(Class<T> clazz){
		entityManagerFactory = SessionManager.getInstance().getSessionFactory();
		this.clazz = clazz;
	}
	
	protected EntityManager getEntityManager(){		
		return entityManagerFactory.createEntityManager();
	}
	
	protected void closeSession(EntityManager em, EntityTransaction t){
		if (em == null) return;
		
		if (em.isOpen()){
			if (t != null && t.isActive()){
				t.commit();
			}
			
			em.close();
		}
		
		em = null;
	}
	
	public List<T> getEntities(){
		return getEntities(null, null, null);
	}
	
	public List<T> getEntities(String orderField){
		return getEntities(null, null, orderField);
	}

	public List<T> getEntities(String field, Object value){
		return getEntities(field, value, null);
	}

	public List<T> getEntities(String field, Object value, String orderField){
		EntityManager em = getEntityManager();
		EntityTransaction t = em.getTransaction();
		try{
			t.begin();
			String strQuery = "select c from " + clazz.getCanonicalName() + " c";
			
			if (field != null) strQuery += " where c." + field + " = " + value;
			
			if (orderField != null) strQuery += " order by c." + orderField;
			
			System.out.println(strQuery);
			TypedQuery<T> q = (TypedQuery<T>) em.createQuery(strQuery, clazz);
			em.getTransaction().commit();
			
			return q.getResultList();
		} finally{
			closeSession(em, t);
		}
	}
	
	public void saveEntity(T entity){
		EntityManager em = getEntityManager();
		EntityTransaction t = em.getTransaction();
		
		try{
			t.begin();
			em.persist(entity);				
		} finally{
			closeSession(em, t);
		}		
	}
	
	public void updateEntity(T entity){
		EntityManager em = getEntityManager();
		EntityTransaction t = em.getTransaction();
		
		try{
			t.begin();
			em.merge(entity);				
		} finally{
			closeSession(em, t);
		}		
	}

	public void deleteEntity(T entity){
		EntityManager em = getEntityManager();
		EntityTransaction t = em.getTransaction();
		
		try{
			t.begin();
			em.remove(entity);
		} finally{
			closeSession(em, t);
		}		
	}	
	
	public void deleteEntity(Long id){
		EntityManager em = getEntityManager();
		EntityTransaction t = em.getTransaction();
		
		try{
			T entity = em.find(clazz, id);
			t.begin();
			em.remove(entity);
		} finally{
			closeSession(em, t);
		}		
	}
	
	public T getEntity(Long id){
		EntityManager em = getEntityManager();
		T entity;
		
		try{
			entity = em.find(clazz, id);
		} finally{
			closeSession(em, null);
		}
		
		return entity;
	}
}
