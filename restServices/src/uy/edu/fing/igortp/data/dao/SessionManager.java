package uy.edu.fing.igortp.data.dao;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class SessionManager {
	private static SessionManager instance = new SessionManager();
	private EntityManagerFactory entityManagerFactory;
	
	private SessionManager(){
		entityManagerFactory = Persistence.createEntityManagerFactory("igortp");
	}
	
	public static SessionManager getInstance(){
		return instance;
	}
	
	public EntityManagerFactory getSessionFactory(){
		return entityManagerFactory;
	}
}
