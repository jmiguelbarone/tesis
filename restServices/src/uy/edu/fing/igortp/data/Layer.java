package uy.edu.fing.igortp.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="layer")
@Entity
public class Layer {
	public static enum LayerType {WMS, GML, JSON};
	@Id
	@GeneratedValue
	private Long id;
	private String name;
	private String title;
	private LayerType type;
	private String store;
	private Boolean baseLayer;
	
	public Layer(){
	}

	public Layer(String n, LayerType lt, String u){
		name = n;
		type = lt;
		store = u;
	}
	
	public Long getId() {
		return id;
	}

	@XmlElement
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	@XmlElement
	public void setName(String name) {
		this.name = name;
	}

	
	public String getTitle() {
		return title;
	}

	@XmlElement
	public void setTitle(String title) {
		this.title = title;
	}

	public LayerType getType() {
		return type;
	}

	@XmlElement
	public void setType(LayerType type) {
		this.type = type;
	}

	public String getStore() {
		return store;
	}

	@XmlElement
	public void setStore(String uri) {
		this.store = uri;
	}

//	public Project getProject() {
//		return project;
//	}
//
//	public void setProject(Project project) {
//		this.project = project;
//	}

	public Boolean isBaseLayer() {
		return baseLayer;
	}

	public void setBaseLayer(Boolean baseLayer) {
		this.baseLayer = baseLayer;
	}

}
