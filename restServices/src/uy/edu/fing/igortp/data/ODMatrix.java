package uy.edu.fing.igortp.data;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="od_matrix")
@Entity
public class ODMatrix {
	@Id
	@GeneratedValue
	private Long id;
	@OneToOne(fetch=FetchType.EAGER)
	private Project project;
	@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL, mappedBy="matrix")
	private Set<ODElement> elements;
	
	public Long getId() {
		return id;
	}
	
	@XmlElement
	public void setId(Long id) {
		this.id = id;
	}

	public Project getProject() {
		return project;
	}

	@XmlElement
	public void setProject(Project project) {
		this.project = project;
	}

	public Set<ODElement> getElements() {
		return elements;
	}

	@XmlElement(name="od_element")
	public void setElements(Set<ODElement> elements) {
		this.elements = elements;
	}

	@Override
	public String toString() {
		return "ODMatrix [id=" + id + ", project=" + project + "]";
	}
	
	
}
